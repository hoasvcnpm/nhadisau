$(document).ready(function() {
  $('.slider-tour-id').owlCarousel({
    loop: true,
    margin: 10,
    autoplayHoverPause:false,
     autoplay: 6000,
    smartSpeed: 700,
    responsiveClass: true,
    navText: [ '<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>' ],
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false,
        margin: 30
      },
      1000: {
        items: 3,
        nav: true,
        loop: false,
        margin: 30
      },
      1300: {
        items: 3,
        nav: true,
        loop: false,
        margin: 30
      }
    }
  })
})





$(window).scroll(function(){
  var sticky = $('header'),
      scroll = $(window).scrollTop();

  if (scroll >= 170) sticky.addClass('scroll-to-top');
  else sticky.removeClass('scroll-to-top');
});
